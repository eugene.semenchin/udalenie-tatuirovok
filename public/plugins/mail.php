<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$name = trim(strip_tags($_POST['name']));
$phone = trim(strip_tags($_POST['phone']));
$messengers = trim(strip_tags(implode(', ', $_POST['messengers'])));
$services = trim(strip_tags(implode(', ', $_POST['services'])));
$usercomment = trim(strip_tags($_POST['user_comment']));

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require __DIR__ . '/PHPMailer/src/Exception.php';
require __DIR__ . '/PHPMailer/src/PHPMailer.php';
require __DIR__ . '/PHPMailer/src/SMTP.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.yandex.ru';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'мыло отправителя';                     //SMTP username
    $mail->Password   = 'пароль от мыла отправителя';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom('мыло отправителя');
    $mail->addAddress('кому отправить');     //Add a recipient
    
    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Заявка на запись с сайта';
    $mail->Body    = '<h1>Новая запись с сайта</h1>';
    $mail->Body    .= '<p>Имя - ' . $name . '</p>';
    $mail->Body    .= '<p>Телефон - ' . $phone . '</p>';
    $mail->Body    .= '<p>Мессенджеры - ' . $messengers . '</p>';
    $mail->Body    .= '<p>Услуги - ' . $services . '</p>';
    $mail->Body    .= '<p>Комментарий - ' . $usercomment . '</p>';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

