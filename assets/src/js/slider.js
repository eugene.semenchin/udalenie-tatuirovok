var swiper = new Swiper(".reviews__slider", {
    effect: 'fade',
    speed: 500,
    pagination: {
      el: ".swiper-pagination",
      type: "fraction",
    },

    navigation: {
      nextEl: ".reviews-button-next",
      prevEl: ".reviews-button-prev",
},});

var swiper = new Swiper(".articles__slider", {
  slidesPerView: 3,
  spaceBetween: 30,
  loop: true,
  loopFillGroupWithBlank: true,

  navigation: {
    nextEl: ".articles-button-next",
    prevEl: ".articles-button-prev",
  },

  breakpoints: {
    768: {
      slidesPerView: 3,
    },

    576: {
      slidesPerView: 2,
    },

    375: {
      slidesPerView: 1,
    },

    320: {
      slidesPerView: 1,
    }
  }
});

var swiper = new Swiper(".gallery__slider", {
  loop: true,
  slidesPerView: 3,
  autoplay: {
    delay: 2500,
    disableOnInteraction: false,
  },

  breakpoints: {
    1024: {
      spaceBetween: 30,
    },

    768: {
      slidesPerView: 3,
      spaceBetween: 20,
    },

    576: {
      slidesPerView: 2,
      spaceBetween: 20,
    },

    320: {
      slidesPerView: 1,
      spaceBetween: 10,
    },
  },
});