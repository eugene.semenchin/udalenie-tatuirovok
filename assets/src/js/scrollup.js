window.onscroll = () => {
    toggleTopButton();
  }

  const scrollToUp = document.querySelector('.scrollup');

  function scrollToTop(){
    window.scrollTo({top: 0, behavior: 'smooth'});
  }
  
  function toggleTopButton() {
    if (document.body.scrollTop > 2500 ||
        document.documentElement.scrollTop > 2500) {
        scrollToUp.classList.remove('scrollup--hide');
    } else {
        scrollToUp.classList.add('scrollup--hide');
    }
  }