const descriptionBlock = document.querySelector('.description-price__wrapper');
const showContent = document.querySelector('.description-price__button');

if (document.getElementsByClassName('section__price').length) {
    showContent.addEventListener('click', function () {
        descriptionBlock.classList.toggle('description-price__wrapper--show');
    
        if (descriptionBlock.classList.contains('description-price__wrapper--show')) {
        showContent.innerText='Свернуть';
        } else {
            showContent.innerText='Показать все';
        }
    });
}



