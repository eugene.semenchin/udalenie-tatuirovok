let selector = document.querySelectorAll('input[type="tel"]');
let im = new Inputmask('+7 (999) 999-99-99');
im.mask(selector);

const modal = document.querySelector('.modal')
const submitForm = document.querySelector('.submit__form');
const closeModal = document.querySelector('.close__button');

closeModal.addEventListener('click', function () {
	modal.classList.remove('modal--active');
});


let validateForms = function(selector, rules, successModal, yaGoal) {
	new window.JustValidate(selector, {
		rules: rules,
		submitHandler: function(form) {
            let formData = new FormData(form);

            let xhr = new XMLHttpRequest();
		
            xhr.onreadystatechange = function() {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						console.log('Отправлено');

						submitForm.addEventListener('click', function () {
							modal.classList.add('modal--active');
						});

					}
				}
			}

            xhr.open('POST', '/plugins/mail.php', true);

            xhr.send(formData);

            form.reset();
		}
	});
}

validateForms('.form', {tel: {required: true} }, {name: {required: true} })







